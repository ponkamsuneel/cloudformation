# Amazon Cloud Formation

CloudFormation template for provisioning a simple virtual machine on Amazon Web Services (AWS).

## Requirements

* AWS Account
* AWS EC2 KeyPair 
* (Optional) AWS CLI

## Usage

Deploy using the [CloudFormation designer](https://aws.amazon.com/cloudformation/) or by using the AWS CLI:

```bash
# verify with cfn-lint (macOS: `brew install cfn-lint`)
cfn-lint formation.yaml

# Shell variables
STACK_NAME="DEMO-CLUSTER"
KEY_NAME="VMKey-NorthCalifornia"
STACK_POSTFIX="vm-1"

# Create CloudFormation Stack with this template
aws --region us-west-1 cloudformation create-stack --stack-name $STACK_NAME --template-body file://PATH/TO/formation.yaml --parameters ParameterKey=KeyName,ParameterValue=$KEY_NAME ParameterKey=StackPostfix,ParameterValue=$STACK_POSTFIX 
```

## SSH into virtual machine

SSH into the virtual machine using the keyfile and the public provisioned Elastic IP Address.

```bash
ssh -i KEYFILE.pem ec2-user@x.x.x.x
```

## Blog and Presentations

* https://skofgar.ch/dev/2020/11/deploying-a-vm-using-aws-cloud-formation/
* https://gitlab.com/skofgar/presentations/-/tree/master/2021-cloudworld-aws-cloudformation

## Documentation

* https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/deploying.applications.html
